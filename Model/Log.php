<?php


class Log{
	
	private $userName = null;
	private $totalDistance = 0;

	function __construct($skier, $clubId = null, $seasonYear, $dbModel){

		//Read Skiers data

		$this->userName = $skier->getAttribute("userName");
		echo "processing user: " . $this->userName . "\n";


		/*Must add season before log*/
		//Adding skier to a season

		$stmt3 = $dbModel->db->prepare('INSERT INTO Season (Username, SYear, ClubId)' . 'VALUES(:usr, :seasonYear, :clubId)');
		$stmt3->bindValue(':usr', $this->userName);
		$stmt3->bindValue(':seasonYear', $seasonYear);
		$stmt3->bindValue(':clubId', $clubId);

		$success = $stmt3->execute();

		//get all entry in log
		$entryResult = $skier->getElementsByTagName("Entry");

		foreach($entryResult as $entry){
			$dateValue = $entry->getElementsByTagName("Date")->item(0)->nodeValue;
			$time = strtotime($dateValue);
			$date = date("Y-m-d", $time);
			$area = $entry->getElementsByTagName("Area")->item(0)->nodeValue;
			$distance = $entry->getElementsByTagName("Distance")->item(0)->nodeValue;
			$this->totalDistance += $distance;

			echo "Username: " . $this->userName . " - date: " . $date . " - area: " . $area . " - distance: " . $distance . "\n\n";

			try{
				$stmt = $dbModel->db->prepare('INSERT INTO Log (Username, InSeason, Location, AtDate, Distance)' . 'VALUES(:usr, :inSeason, :Loc, :AtDate, :Distance)');
				$stmt->bindValue(':usr', $this->userName);
				$stmt->bindValue(':inSeason', $seasonYear);
				$stmt->bindValue(':AtDate', $date);
				$stmt->bindValue(':Loc', $area);
				$stmt->bindValue(':Distance', $distance);

				$success = $stmt->execute();


			}catch(PDOException $e){
				echo "Database failed: " . $e->getMessage();
			}
		}

		


		//Adding totaldistance for a user in this season

		$stmt2 = $dbModel->db->prepare('INSERT INTO TotalDistance (Username, SeasonYear, TotDist)' . 'VALUES(:usr, :seasonYear, :totalDistance)');
		$stmt2->bindValue(':usr', $this->userName);
		$stmt2->bindValue(':seasonYear', $seasonYear);
		$stmt2->bindValue(':totalDistance', $this->totalDistance);

		$success2 = $stmt2->execute();




	}	

}


?>