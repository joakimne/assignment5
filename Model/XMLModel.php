<?php

include_once("Model/Club.php");
include_once("Model/Skier.php");
include_once("Model/Season.php");
include_once("Model/db.php");


class XMLModel{

	private $docURL = "Source/xml.xml";
	private $doc = null;
	private $xpath = null;
	private $database = null;


	public function __construct(){

		$this->database = new DBModel();
		$this->doc = new DOMDocument();

		if(!$this->doc->load($this->docURL)){
			echo "Something wrong with reading xml document";
			//error
		}else{
			//process
			//echo "Successfully read the xml document";
			$this->xpath = new DOMXpath($this->doc);
			$this->readClubs();
			$this->readSkiers();
			$this->readSeason();

		}

	}
	

	private function readClubs(){

		if($this->doc){

			$clubNodes = $this->doc->getElementsByTagName("Club");

			for ($i = 0; $i < $clubNodes->length; $i++) {
				$obj = new Club($clubNodes[$i], $this->database);
			}



		}else{
			echo "doc object doesnt exists";
		}


	}

	private function readSkiers(){

		$query = '//SkierLogs/Skiers/Skier';
		$xpathResult = $this->xpath->query($query);

		foreach($xpathResult as $skierNode){

			$skier = new Skier($skierNode, $this->database);
		}

	}

	private function readSeason(){

		$query = '//SkierLogs/Season';
		$xpathResult = $this->xpath->query($query);

		foreach ($xpathResult as $seasonNode) {

			$season = new Season($seasonNode, $this->database, $this->xpath);


		}

	}


}




?>
