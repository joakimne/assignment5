<?php

class Club{


	private $id = null;
	private $name = null;
	private $city = null;
	private $county = null;


	public function __construct($clubNode, $dbModel){

		if($clubNode){

			$this->id = $clubNode->getAttribute("id");
			$this->name = $clubNode->getElementsByTagName("Name")->item(0)->nodeValue;
			$this->city = $clubNode->getElementsByTagName("City")->item(0)->nodeValue;
			$this->county = $clubNode->getElementsByTagName("County")->item(0)->nodeValue;

			//Putting it in database

			try{
				$stmt = $dbModel->db->prepare('INSERT INTO Club (Id, Name, City, Country)' . 'VALUES(:id, :name, :city, :county)');
				$stmt->bindValue(':id', $this->id);
				$stmt->bindValue(':name', $this->name);
				$stmt->bindValue(':city', $this->city);
				$stmt->bindValue(':county', $this->county);

				$success = $stmt->execute();
			}catch(PDOException $e){
				echo "Database failed: " . $e->getMessage();
			}
		}

	}

}


?>