
<?php

class Skier{

	private $username = null;
	private $firstName = null;
	private $lastName = null;
	private $yearOfBirth = null;


	public function __construct($skierNode, $dbModel){

		if($skierNode){

			$this->username = $skierNode->getAttribute("userName");
			$this->firstName = $skierNode->getElementsByTagName("FirstName")->item(0)->nodeValue;
			$this->lastName = $skierNode->getElementsByTagName("LastName")->item(0)->nodeValue;
			$this->yearOfBirth = $skierNode->getElementsByTagName("YearOfBirth")->item(0)->nodeValue;
		

			try{
				$stmt = $dbModel->db->prepare('INSERT INTO Skier (Username, FirstName, LastName, YearOfBirth)' . 'VALUES(:usr, :fname, :lname, :yob)');
				$stmt->bindValue(':usr', $this->username);
				$stmt->bindValue(':fname', $this->firstName);
				$stmt->bindValue(':lname', $this->lastName);
				$stmt->bindValue(':yob', $this->yearOfBirth);

				$success = $stmt->execute();
			}catch(PDOException $e){
				echo "Database failed: " . $e->getMessage();
			}

		}else{
			echo "No SkierNode is defined!";
		}



	}



}


?>