<?php

include_once("Model/Log.php");

class Season{



	public $fallYear = 0;


	function __construct($season, $dbModel){

		if($season){

			$this->fallYear = $season->getAttribute("fallYear");

			//get skiers in season
			//read skiers in a specific club

			//$xpath = new DOMXPath($season);
			//$query = "Skiers";
			$xpathClub =  $season->getElementsByTagName("Skiers"); //$xpath->query($query); //[Skiers,skiers,skiers]

			//Foreach club in season
			foreach ($xpathClub as $club) {

				$clubId = null;

				if($club->hasAttribute("clubId")){
					$clubId = $club->getAttribute("clubId");
				}
				//each club have multiple skier

				//$subQuery = "Skier"; //Get all skier nodes
				//$subXpath = new DOMXPath($club);
				//$subXpathResult = $subXpath->query($subQuery); //Contains skier or a specific club/or not club in a season
				$subQuery = $club->getElementsByTagName("Skier");


				//Skier reads it's own log
				foreach ($subQuery as $skier) {
					$skierLog = new Log($skier, $clubId, $this->fallYear, $dbModel);
				}
				

			}





		}else{

			echo "No season is defined";
		}


	}



}


?>