<?php

include_once("constants.php");


class DBModel{

	public $db = null;


	public function __construct($db = null)  
    {  
	    if ($this->db) //$db
		{
			$this->db = $db;
		}
		else
		{
            // Create PDO connection
            //Try connection. Catch if it fails
            try{
                $this->db = new PDO('mysql:host=' . DB_HOST . ';charset=utf8;dbname=' . DB_NAME, DB_USER, DB_PASS);
                $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }catch(PDOException $e){
                //Catching db connection avoids termination of the script and leaving a back trace to the user. Might releav username and password.
                //Give the user a normal error message, PDOException-message should be logged to a file or something for review. But we haven't implemented that yet.
                echo "Database: " . $e->getMessage();
                //throw new PDOException("Database connection failed");
            }
		}
    }
}


?>