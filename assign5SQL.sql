	DROP SCHEMA

	IF EXISTS assignmentfive;
	    CREATE DATABASE assignmentFive COLLATE = utf8_danish_ci;
	USE assignmentfive;



	CREATE TABLE Club(
		Id VARCHAR(25) NOT NULL,
		Name VARCHAR(100) NOT NULL,
		City VARCHAR(100) NOT NULL,
		Country VARCHAR(100) NOT NULL,
		PRIMARY KEY(id)
	);

	CREATE TABLE Skier(
		Username VARCHAR(250) NOT NULL,
		FirstName VARCHAR(250) NOT NULL,
		LastName VARCHAR(250) NOT NULL,
		YearOfBirth int NOT NULL,
		PRIMARY KEY(Username)
	);

	CREATE TABLE Season(
		Username VARCHAR(250) NOT NULL,
		SYear int NOT NULL,
		ClubId VARCHAR(25),
		PRIMARY KEY (Username, SYear),

		FOREIGN KEY (ClubId)
		REFERENCES Club(Id)
		ON DELETE RESTRICT
		ON UPDATE CASCADE,

		FOREIGN KEY (Username)
		REFERENCES Skier(Username)
		ON DELETE RESTRICT
		ON UPDATE CASCADE
	);


		CREATE TABLE Log(
			Username VARCHAR(250) NOT NULL,
			InSeason int NOT NULL,
			Location VARCHAR(100) NOT NULL,
			AtDate DATE NOT NULL,
			Distance int NOT NULL,

			PRIMARY KEY (Username, InSeason, AtDate),

			FOREIGN KEY (Username)
			REFERENCES Skier(Username)
			ON DELETE RESTRICT
			ON UPDATE CASCADE,
		
			FOREIGN KEY (Username, InSeason)
			REFERENCES Season(Username, SYear)
			ON DELETE RESTRICT
			ON UPDATE CASCADE
		);


	CREATE TABLE TotalDistance(
		Username VARCHAR(250) NOT NULL,
		SeasonYear int NOT NULL,
		TotDist int NOT NULL,
		PRIMARY KEY (Username, SeasonYear),

		FOREIGN KEY (Username)
		REFERENCES Skier(Username)
		ON DELETE RESTRICT
		ON UPDATE CASCADE,

		FOREIGN KEY (Username, SeasonYear)
		REFERENCES Season(Username, SYear)
		ON DELETE RESTRICT
		ON UPDATE CASCADE

	);
		

